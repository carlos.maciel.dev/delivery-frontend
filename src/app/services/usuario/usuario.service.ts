import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { UserRegistred } from 'src/app/model/userRegistred';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private db: AngularFireDatabase) { }

  insert(usuario: UserRegistred, uid: string): void {
    this.db.object('usuario/' + uid).set(usuario);

  }

  getAll(): Observable<any[]> {
    return this.db.list('usuario/', ref => ref.orderByChild('id')).valueChanges();
  }




}
