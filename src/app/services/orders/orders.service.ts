import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Order } from 'src/app/model/order';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private db: AngularFireDatabase) { }

  insert(order: Order): void {
    this.db.list('orders/').push(order);

  }

  getAll(id): Observable<any[]> {
    return this.db.list('orders/', ref => ref.orderByChild('user/'+ id)).valueChanges();
  }

  // update(id): void {
  //   return this.db.object('orders/').set('status');
  // }


}
