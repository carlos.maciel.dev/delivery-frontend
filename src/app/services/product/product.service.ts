import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Product } from 'src/app/model/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFireDatabase) { }

  insert(product: Product): void {
    this.db.list('products/').push(product);

  }

  getAll(): Observable<any[]> {
    return this.db.list('products/', ref => ref.orderByChild('id')).valueChanges();
  }
}
