import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './containers/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { ListProductsComponent } from './containers/list-products/list-products.component';
import { DescriptionProductComponent } from './containers/description-product/description-product.component';
import {MatChipsModule} from '@angular/material/chips';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import {  AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { SignInComponent } from './containers/login/sign-in/sign-in.component';
import { SignUpComponent } from './containers/login/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './containers/login/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './containers/login/verify-email/verify-email.component';
import { AuthService } from './services/login/auth.service';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ValidationProductComponent } from './containers/validation-product/validation-product.component';
import { AddressDeliveryComponent } from './containers/address-delivery/address-delivery.component';
import { ToobarComponent } from './shared/toobar/toobar.component';
import {MatMenuModule} from '@angular/material/menu';
import {FlexLayoutModule} from '@angular/flex-layout';
import { ProfileComponent } from './containers/profile/profile.component';
import { MyOrdersComponent } from './containers/my-orders/my-orders.component';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import { ConfirmationAddressComponent } from './containers/confirmation-address/confirmation-address.component';
import { CancelConfirmationOrderComponent } from './containers/cancel-confirmation-order/cancel-confirmation-order.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListProductsComponent,
    DescriptionProductComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    ValidationProductComponent,
    AddressDeliveryComponent,
    ToobarComponent,
    ProfileComponent,
    MyOrdersComponent,
    ConfirmationAddressComponent,
    CancelConfirmationOrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    MatChipsModule,
    MatToolbarModule,
    MatMenuModule,
    MatListModule,
    MatDialogModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    FlexLayoutModule

  ],
  entryComponents: [
    ConfirmationAddressComponent,
    DescriptionProductComponent
  ],
  providers: [AuthService, {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}],
  bootstrap: [AppComponent]
})
export class AppModule { }
