import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './containers/home/home.component';
import { ListProductsComponent } from './containers/list-products/list-products.component';
import { SignInComponent } from './containers/login/sign-in/sign-in.component';
import { SignUpComponent } from './containers/login/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './containers/login/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './containers/login/verify-email/verify-email.component';
import { AuthGuard } from './services/login/auth.guard';
import { DescriptionProductComponent } from './containers/description-product/description-product.component';
import { ValidationProductComponent } from './containers/validation-product/validation-product.component';
import { AddressDeliveryComponent } from './containers/address-delivery/address-delivery.component';
import { ProfileComponent } from './containers/profile/profile.component';
import { MyOrdersComponent } from './containers/my-orders/my-orders.component';
import { CancelConfirmationOrderComponent } from './containers/cancel-confirmation-order/cancel-confirmation-order.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'home', component: HomeComponent},
  { path: 'products', component: ListProductsComponent},
  { path: 'detail', component: DescriptionProductComponent},
  { path: 'validation', component: ValidationProductComponent},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  { path: 'address', component: AddressDeliveryComponent},
  { path: 'my-orders', component: MyOrdersComponent},
  { path: 'confirmation-cancel', component: CancelConfirmationOrderComponent},

  // { path: 'buy-product', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
