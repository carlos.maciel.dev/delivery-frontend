import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/login/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  form: FormGroup;
  hide = true;
  email: any;
  password: any;

  constructor(private router: Router,public authService: AuthService) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })

  }
  acessar(){
    this.email = this.form.get('email').value;
    this.password = this.form.get('password').value;

    this.authService.SignIn(this.email, this.password);
    // this.router.navigate(['principal']);
  }
  irNewUser(){
    this.router.navigate(['register-user']);
  }

}
