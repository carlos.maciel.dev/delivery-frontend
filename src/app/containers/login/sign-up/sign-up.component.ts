import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/login/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  hide = true;
  selected = 'M';
  email: any;
  password: any;
  celular: any;
  displayName: any;
  verifyLogged: any;
  idUser: any;

  constructor(private router: Router, private authenticationService: AuthService, private service: UsuarioService) { }

  ngOnInit() {
    this.verifyLogged = localStorage.getItem('user');
    const userLogger = localStorage.getItem('userLogged');
    const user = this.convertStringJson(userLogger);
    this.idUser = user.id;
    if (this.verifyLogged !== ''){
      this.form = new FormGroup({
        id: new FormControl(user.id),
        name: new FormControl(user.name, [Validators.required]),
        genero: new FormControl(user.genero, [Validators.required]),
        telefone: new FormControl(user.telefone, [Validators.required]),
        email: new FormControl(user.email),
        cep: new FormControl(user.cep, [Validators.required]),
        rua: new FormControl(user.rua, [Validators.required]),
        numero: new FormControl(user.numero, [Validators.required]),
        complemento: new FormControl(user.complemento),
        bairro: new FormControl(user.bairro, [Validators.required]),
      });

    } else {

    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      genero: new FormControl('', [Validators.required]),
      telefone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.minLength(6), Validators.required]),
      cep: new FormControl('', [Validators.required]),
      rua: new FormControl('', [Validators.required]),
      numero: new FormControl('', [Validators.required]),
      complemento: new FormControl(''),
      bairro: new FormControl('', [Validators.required]),
    });
  }
  }

  //converte json
  convertStringJson(obj) {
    const converted = JSON.parse(obj);
    return converted;
  }

  backTo() {
    this.router.navigate(['sign-in']);

  }

  registrar() {

    this.email = this.form.get('email').value;
    this.password = this.form.get('password').value;

    const vlrForm = this.form.value;
    console.log(vlrForm);
    console.log(this.displayName)
    this.authenticationService.SignUp(this.email, this.password, vlrForm);

    // this.router.navigate(['usuarioRegistrado']);
  }

  alterar(){
    const vlrForm = this.form.value;
    this.service.insert(vlrForm, this.idUser);

    this.router.navigate(['profile']);
  }


}
