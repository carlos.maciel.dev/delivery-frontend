import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product/product.service';
import { Product } from 'src/app/model/product';
import { MatDialog } from '@angular/material/dialog';
import { DescriptionProductComponent } from '../description-product/description-product.component';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  selected = '1';
  products: Product[];
  constructor(private router: Router, private productService: ProductService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.productService.getAll().subscribe(resp => {
      this.products = resp;
    });
  }

  detailProduct(product) {
    const dialogRef = this.dialog.open(DescriptionProductComponent, {
      data: product,
      width: '100%',
      height: '100%'

    });

    // dialogRef.afterClosed().subscribe(result => {
    //  if (result === true) {
    //   // this.orderService.insert()
    //   this.router.navigate(['validation']);


    //  } else {
    //   this.router.navigate(['address']);
    //  }
    // });
    // this.router.navigate(['/detail/']);
  }

}
