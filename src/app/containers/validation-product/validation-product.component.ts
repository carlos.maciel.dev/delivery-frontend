import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-validation-product',
  templateUrl: './validation-product.component.html',
  styleUrls: ['./validation-product.component.css']
})
export class ValidationProductComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  comprar() {
    this.router.navigate(['products']);
  }

  visualizarStatus() {
    this.router.navigate(['my-orders']);
  }


}
