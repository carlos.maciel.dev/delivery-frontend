import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { City } from 'src/app/model/city';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map, startWith } from 'rxjs/operators';
import { ProductService } from 'src/app/services/product/product.service';
import { Product } from 'src/app/model/product';
import { Image } from 'src/app/model/image';
import { Provider } from 'src/app/model/provider';
import { UserRegistred } from 'src/app/model/userRegistred';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formSearch = new FormGroup({
    myControl: new FormControl('')
  });

  options: City[] = [{ name: 'Manaus' }];
  filteredOptions: Observable<City[]>;
  choice: any;
  product: any;
  image: Image[];
  provider: any;
  user: any;

  products: Product;
  constructor(private router: Router, private productSevice: ProductService) { }

  ngOnInit() {
    this.filteredOptions = this.formSearch.get('myControl').valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.options.slice())
      );


    // this.user = {
    //   id: 'wxuZ5219mXQXrREDlyuunUR7qYH2',
    //   email: 'carlosbeto1996@gmail.com',
    //   name: 'Carlos Maciel',
    //   password: '',
    //   phone: '92993323027',
    //   genre: 'M',
    //   cep: '69027270',
    //   rua: 'Rua da Cachoeira',
    //   numero: '68',
    //   complemento: '',
    //   bairro: 'São Raimundo'
    // };

    // this.image = [{
    //   // tslint:disable-next-line: max-line-length
    //   src: 'https://http2.mlstatic.com/relogio-digital-unisex-smartwhatch-original-android-faz-liga-D_NQ_NP_848204-MLB31197842789_062019-F.jpg',
    //   alt: 'primeira'
    // },
    // {
    //   // tslint:disable-next-line: max-line-length
    //   src: 'https://http2.mlstatic.com/relogio-digital-unisex-smartwhatch-original-android-faz-liga-D_NQ_NP_848204-MLB31197842789_062019-F.jpg',
    //   alt: 'segunda'
    // },
    // {
    //   // tslint:disable-next-line: max-line-length
    //   src: 'https://http2.mlstatic.com/relogio-digital-unisex-smartwhatch-original-android-faz-liga-D_NQ_NP_848204-MLB31197842789_062019-F.jpg',
    //   alt: 'terceira'
    // }];

    // this.provider = {
    //   id: '1',
    //   user: this.user
    // };

    // this.product = {
    //   id: '1',
    //   name: 'Relogio Masculino',
    //   title: 'Relogio Digital Masculino',
    //   category: 'Relogios',
    //   price: '200',
    //   condition: 'Novo',
    //   description: 'Relógio digital masculino novo',
    //   datePublic: '15/07/2020',
    //   status: 'ATIVO',
    //   images: this.image,
    //   year: '2020',
    //   salesman: 'Carlos Maciel',
    //   provider: this.provider,
    // };

    // this.products = this.product;
    // this.productSevice.insert(this.products);
  }

  private _filter(name: string): City[] {
    const filterValue = name.toLowerCase();
    this.choice = filterValue;

    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  search() {
    const city = this.choice;
    console.log(city)
    // alert('em desenvolvimento');
    this.router.navigate(['/products/']);
  }



}
