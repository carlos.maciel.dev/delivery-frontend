import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationAddressComponent } from '../confirmation-address/confirmation-address.component';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { Product } from 'src/app/model/product';
import { Address } from 'src/app/model/address';

@Component({
  selector: 'app-description-product',
  templateUrl: './description-product.component.html',
  styleUrls: ['./description-product.component.css']
})
export class DescriptionProductComponent implements OnInit {

  constructor(private router: Router, public dialog: MatDialog, private orderService: OrdersService,
    @Inject(MAT_DIALOG_DATA) public data: Product) { }
  user: any;
  userLogged: boolean;
  verifyUserLogged: any;
  productChoice: Product;
  order: any;
  date = new Date();
  address: any;
  usuarioAddress: Address;

  ngOnInit() {

    this.productChoice = this.data;
    this.user = localStorage.getItem('user');
    if (this.user !== '') {
      this.verifyUserLogged = true;
      const userLoggedLocal = localStorage.getItem('userLogged');
      this.userLogged = this.convertStringJson(userLoggedLocal);
      this.usuarioAddress = this.convertStringJson(userLoggedLocal);
    } else {
      this.verifyUserLogged = false;
    }

  }

  openDialog() {
    this.address = {
      bairro: this.usuarioAddress.bairro,
      cep: this.usuarioAddress.cep,
      complemento: this.usuarioAddress.complemento,
      numero: this.usuarioAddress.numero,
      rua: this.usuarioAddress.rua
    }

    this.order = {
      priceFinal: this.productChoice.price,
      dateCreate: this.date.getDay() + '/' + this.date.getMonth() + '/' + this.date.getFullYear(),
      user: this.userLogged,
      product: this.productChoice,
      address: this.address,
      status: 'Aguardando Aprovação do Fornecedor'
    }
    this.dialog.closeAll();
    // console.log(this.userLogged);

    const dialogRef = this.dialog.open(ConfirmationAddressComponent, {
      data: this.userLogged
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.orderService.insert(this.order);
        this.router.navigate(['validation']);


      } else {
        this.router.navigate(['profile']);
      }
    });
  }

  login() {
    this.dialog.closeAll();
    // this.router.navigate(['validation']);
    this.router.navigate(['sign-in']);
    // alert('em desenvolvimento');
  }

  //converte json
  convertStringJson(obj) {
    const converted = JSON.parse(obj);
    return converted;
  }
  //transformar string
  convertJsonString(obj) {
    const convertedString = JSON.stringify(obj);
    return convertedString;
  }

  quit() {
    this.dialog.closeAll();
  }

  otherProduct(){
    this.dialog.closeAll();
    // this.router.navigate(['validation']);
    this.router.navigate(['products']);
  }

}
