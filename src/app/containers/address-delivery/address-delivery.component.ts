import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/login/auth.service';

@Component({
  selector: 'app-address-delivery',
  templateUrl: './address-delivery.component.html',
  styleUrls: ['./address-delivery.component.css']
})
export class AddressDeliveryComponent implements OnInit {
  form: FormGroup;
  hide = true;
  selected = 'M';
  idUser: number;
  email: any;
  password: any;
  celular: any;

  constructor(private router: Router, private authenticationService: AuthService) { }

  ngOnInit() {

    this.form = new FormGroup({
      cep: new FormControl('', [Validators.required]),
      rua: new FormControl('', [Validators.required]),
      numero: new FormControl('', [Validators.required]),
      complemento: new FormControl(''),
      bairro: new FormControl('', [Validators.required]),

    });

  }


  registrar() {
    const vlrForm = this.form.value;
    console.log(vlrForm);
    this.router.navigate(['buy']);
  }
}
