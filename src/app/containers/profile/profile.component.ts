import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: any;
  constructor(private router: Router, public service: UsuarioService) { }

  ngOnInit() {
    const userString = localStorage.getItem('user');
    const userJson = this.convertStringJson(userString);

    this.service.getAll().subscribe(result => {
      this.user = result.find(element => element.id === userJson.uid);
    });

  }
  //converte json
  convertStringJson(obj) {
    const converted = JSON.parse(obj);
    return converted;
  }

  editar() {
    this.router.navigate(['register-user']);
  }

  voltar(){
    this.router.navigate(['products']);
  }
}
