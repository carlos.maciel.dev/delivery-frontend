import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelConfirmationOrderComponent } from './cancel-confirmation-order.component';

describe('CancelConfirmationOrderComponent', () => {
  let component: CancelConfirmationOrderComponent;
  let fixture: ComponentFixture<CancelConfirmationOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelConfirmationOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelConfirmationOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
