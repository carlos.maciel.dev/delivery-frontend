import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cancel-confirmation-order',
  templateUrl: './cancel-confirmation-order.component.html',
  styleUrls: ['./cancel-confirmation-order.component.css']
})
export class CancelConfirmationOrderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  comprar() {
    this.router.navigate(['products']);
  }

  visualizarStatus() {
    this.router.navigate(['my-orders']);
  }

}
