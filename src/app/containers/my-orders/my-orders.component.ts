import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { Order } from 'src/app/model/order';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {

  constructor(private router: Router, private ordersService: OrdersService) { }
  orders: Order[];

  user: any;
  userLogged: boolean;
  verifyUserLogged: any;

  order: any;
  date = new Date();
  address: any;

  userId: any;

  ngOnInit() {

    this.user = localStorage.getItem('user');
    if (this.user !== '') {
      this.verifyUserLogged = true;
      const userLoggedLocal = localStorage.getItem('userLogged');
      this.userLogged = this.convertStringJson(userLoggedLocal);
      this.userId = this.convertStringJson(userLoggedLocal);
    } else {
      this.verifyUserLogged = false;
    }
    this.ordersService.getAll(this.userId.id).subscribe(resp=>{
    //  console.log(resp)
     this.orders = resp;
    });
  }

  cancel(){
    this.router.navigate(['confirmation-cancel']);
  }

   //converte json
   convertStringJson(obj) {
    const converted = JSON.parse(obj);
    return converted;
  }
  //transformar string
  convertJsonString(obj) {
    const convertedString = JSON.stringify(obj);
    return convertedString;
  }

}
