import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationAddressComponent } from './confirmation-address.component';

describe('ConfirmationAddressComponent', () => {
  let component: ConfirmationAddressComponent;
  let fixture: ComponentFixture<ConfirmationAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
