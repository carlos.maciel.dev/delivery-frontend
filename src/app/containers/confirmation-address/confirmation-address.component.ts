import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Address } from 'src/app/model/address';

@Component({
  selector: 'app-confirmation-address',
  templateUrl: './confirmation-address.component.html',
  styleUrls: ['./confirmation-address.component.css']
})
export class ConfirmationAddressComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Address) { }

  ngOnInit() {
    console.log(this.data);
  }

  //converte json
  convertStringJson(obj) {
    const converted = JSON.parse(obj);
    return converted;
  }
//transformar string
  convertJsonString(obj) {
    const convertedString = JSON.stringify(obj);
    return convertedString;
  }

}
