import { UserRegistred } from './userRegistred';
import { Product } from './product';
import { Address } from './address';

export interface Order {
  id?: string;
  priceFinal: string;
  dateCreate: string;
  user: UserRegistred;
  product: Product;
  address?: Address;
  status: string;

}
