import { Image } from './image';
import { Provider } from './provider';

export interface Product {
  id: string;
  name: string;
  title: string;
  category: string;
  price: string;
  condition: string;
  description: string;
  datePublic: string;
  status: string;
  images: Image[];
  year: string;
  salesman: string;
  provider: Provider;
}
