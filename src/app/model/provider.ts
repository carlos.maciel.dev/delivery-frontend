import { UserRegistred } from './userRegistred';

export interface Provider {
  id: string;
  user: UserRegistred;
}
