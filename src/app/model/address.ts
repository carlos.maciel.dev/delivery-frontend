export interface Address {
  bairro: string;
  cep: string;
  complemento?: string;
  numero: string;
  rua: string;
}
