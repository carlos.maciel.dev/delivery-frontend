export interface UserRegistred {
  id: string;
  email: string;
  name: string;
  password?: string;
  phone: string;
  genre: string;
  cep: string;
  rua: string;
  numero: string;
  complemento?: string;
  bairro: string;
}
