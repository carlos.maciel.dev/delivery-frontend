import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-toobar',
  templateUrl: './toobar.component.html',
  styleUrls: ['./toobar.component.css']
})
export class ToobarComponent implements OnInit {
  choice: any;
  userLogin: any;
  userName: any;
  constructor(private router: Router, private userService: UsuarioService) { }

  ngOnInit() {
    const user = localStorage.getItem('user');
    if (user !== '') {
      this.choice = true;
      this.userLogin = user;
      console.log(this.userLogin);
      //transformar json
      const obj = this.convertStringJson(this.userLogin);
      //metodo para achar usuario (gambiarra temp kkk)
      this.searchUser(obj.uid);

    } else {
      this.choice = false;
    }
  }

  login() {
    this.router.navigate(['sign-in']);
  }

  searchUser(email) {
    console.log(email)
    this.userService.getAll().subscribe(result => {
      console.log(result)
      const search = result.find(element => element.id === email);
      this.userName = search.name;
      console.log(search);

      const convertedString = this.convertJsonString(search);
      localStorage.setItem('userLogged', convertedString);
    });


  }


//converte json
  convertStringJson(obj) {
    const converted = JSON.parse(obj);
    return converted;
  }
//transformar string
  convertJsonString(obj) {
    const convertedString = JSON.stringify(obj);
    return convertedString;
  }

  singout(){

  }

  profile(){
    this.router.navigate(['profile']);
  }

  home(){
    this.router.navigate(['products']);

  }

  myOrders(){
    this.router.navigate(['my-orders']);
  }

}
